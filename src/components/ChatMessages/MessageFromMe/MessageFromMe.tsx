import React from "react";
import PropTypes from "prop-types";
import "./MessageFromMe.scss";

interface IMessageFromMeProps {
  messageId: string
  text: string;
  likesAmount: number;
  date: string;
  onEdit: (messageId: string) => void;
  onDelete: (messageId: string) => void;
}

function MessageFromMe(props: IMessageFromMeProps) {
  return (
    <div className="message from-me">
      <div className="message__text">{props.text}</div>
      <div className="message__footer">
        <div className="like">
          <span className="material-icons icon-like">favorite</span>
          {props.likesAmount}
        </div>
        <span onClick={() => props.onEdit(props.messageId)} className="material-icons icon-edit">
          edit
        </span>
        <span onClick={() => props.onDelete(props.messageId)} className="material-icons icon-delete">
          delete
        </span>
        <div className="date">{props.date}</div>
      </div>
    </div>
  );
}

MessageFromMe.propTypes = {
  messageId: PropTypes.string,
  text: PropTypes.string,
  likesAmount: PropTypes.number,
  date: PropTypes.string,
  onEdit: PropTypes.func,
  onDelete: PropTypes.func,
};

export default MessageFromMe;
