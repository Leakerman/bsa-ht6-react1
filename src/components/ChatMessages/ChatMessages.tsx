import React, { Fragment, useEffect } from "react";
import "./ChatMessages.scss";
import MessageFromMe from "./MessageFromMe/MessageFromMe";
import MessageFromThem from "./MessageFromThem/MessageFromThem";
import DayDelimiter from "./DayDelimiter/DayDelimiter";
import { IMessage } from "../../interfaces";
import PropTypes from "prop-types";

interface IChatMessagesProps {
  currentUserId: string;
  messages: IMessage[];
  onEdit: (messageId: string) => void;
  onDelete: (messageId: string) => void;
  onLike: (messageId: string) => void;
}

function ChatMessages(props: IChatMessagesProps): JSX.Element {
  useEffect(() => {
    const chatBox = document.querySelector(".chat__messages-box");
    chatBox!.scrollTop = chatBox!.scrollHeight;
  }, []);

  let messagesDate = "";

  return (
    <div className="chat__messages-box">
      {props.messages.map((message) => {
        const date = getMessagesDate(message.createdAt);
        let delimiter = null;
        if (messagesDate !== date) {
          const delimiterDate = new Date(message.createdAt).toLocaleString(
            "en-US",
            {
              month: "long",
              day: "numeric",
            }
          );
          messagesDate = date;
          delimiter = <DayDelimiter date={delimiterDate} />;
        }

        return (
          <Fragment key={message.id + Date.now()}>
            {delimiter}
            {createMessage(
              message,
              props.currentUserId,
              props.onEdit,
              props.onDelete,
              props.onLike
            )}
          </Fragment>
        );
      })}
    </div>
  );
}

function getMessagesDate(date: string) {
  return new Date(date).toLocaleString("en-US", {
    year: "numeric",
    month: "numeric",
    day: "numeric",
  });
}

function createMessage(
  message: IMessage,
  currentUserId: string,
  onEdit: (messageId: string) => void,
  onDelete: (messageId: string) => void,
  onLike: (messageId: string) => void
) {
  const creationTime = getMessageTime(message.createdAt);
  const likesAmount = message.likes!.length;
  const { text, id } = message;
  if (message.userId === currentUserId) {
    return (
      <MessageFromMe
        key={text + Date.now()}
        messageId={id}
        onEdit={onEdit}
        onDelete={onDelete}
        text={text}
        likesAmount={likesAmount}
        date={creationTime}
      />
    );
  } else {
    const isLiked = message.likes.includes(currentUserId);
    const { avatar } = message;

    return (
      <MessageFromThem
        key={avatar + Math.random()}
        messageId={id}
        text={text}
        likesAmount={likesAmount}
        isLiked={isLiked}
        avatar={avatar}
        date={creationTime}
        onLike={onLike}
      />
    );
  }
}

function getMessageTime(date: string) {
  const time = new Date(date);
  return `${time.getHours()}:${time.getMinutes()}`;
}

ChatMessages.propTypes = {
  currentUserId: PropTypes.string,
  messages: PropTypes.array,
  onEdit: PropTypes.func,
  onDelete: PropTypes.func,
  onLike: PropTypes.func,
};

export default ChatMessages;
