import React from "react";
import PropTypes from "prop-types";
import "./MessageFromThem.scss";

interface IMessageFromThemProps {
  messageId: string;
  avatar: string;
  text: string;
  isLiked: boolean;
  likesAmount: number;
  date: string;
  onLike: (messageId: string) => void;
}

function MessageFromThem(props: IMessageFromThemProps) {
  return (
    <div className="message from-them">
      <div>
        <img src={props.avatar} alt="avatar" className="message__avatar" />
        <div className="message__text">{props.text}</div>
      </div>
      <div className="message__footer">
        <div className="like">
          <span onClick={() => props.onLike(props.messageId)} className="material-icons icon-like">
            {props.isLiked ? "favorite" : "favorite_border"}
          </span>
          {props.likesAmount}
        </div>
        <div className="date">{props.date}</div>
      </div>
    </div>
  );
}

MessageFromThem.propTypes = {
  messageId: PropTypes.string,
  avatar: PropTypes.string,
  text: PropTypes.string,
  isLiked: PropTypes.bool,
  likesAmount: PropTypes.number,
  date: PropTypes.string,
  onLike: PropTypes.func,
};

export default MessageFromThem;
