import React from "react";
import "./ChatHeader.scss"

interface IChatHeaderProps {
  chatName: string;
  participants: number;
  messages: number;
  lastMessageDate: string;
}

function ChatHeader(props: IChatHeaderProps): JSX.Element {
  return (
    <div className="chat__header">
      <div className="chat__header-info">
        <span className="chat-name">{props.chatName}</span>
        <span className="chat-participants">
          {props.participants} participants
        </span>
        <span className="chat-messages">{props.messages} messages</span>
      </div>
      <div className="chat__last-message-date">
          last message at {props.lastMessageDate}
      </div>
    </div>
  );
};

export default ChatHeader;
