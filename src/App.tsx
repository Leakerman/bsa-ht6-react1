import React from "react";
import Layout from "./hoc/Layout/Layout";
import { Route, Switch } from "react-router-dom";
import Chat from "./containers/Chat/Chat";

function App() {
  const routes = (
    <Switch>
      <Route path="/" component={Chat} />
    </Switch>
  );
  return <Layout>{routes}</Layout>;
}

export default App;
