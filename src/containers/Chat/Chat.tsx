import React, { Component } from "react";
import { v1 as uuid } from "uuid";
import axios from "axios";
import ChatHeader from "../../components/ChatHeader/ChatHeader";
import ChatMessages from "../../components/ChatMessages/ChatMessages";
import "./Chat.scss";
import { IMessage } from "../../interfaces";

interface IState {
  chatName: string;
  participants: number;
  messagesAmount: number;
  lastMessageDate: string;
  pending: boolean;
  users: Map<string, { user: string; avatar: string }> | null;
  messages: IMessage[];
  currentUserId: string;
  messageInputValue: string;
  isEditing: boolean;
  editingMessageId: string;
}

export default class Chat extends Component {
  componentDidMount() {
    axios
      .get("https://api.npoint.io/b919cb46edac4c74d0a8")
      .then(({ data: messages }) => {
        sortDataByDate(messages);

        const users = createUsersMap(messages);

        const messagesAmount: number = messages.length;
        const participants = users.size;

        const lastMessage = messages[messages.length - 1];
        const lastMessageDate = getMessageTime(lastMessage.createdAt);

        const currentUserId = getActiveUserId(users);

        this.setState({
          participants,
          messagesAmount,
          lastMessageDate,
          pending: false,
          users,
          messages,
          currentUserId,
        });
      })
      .catch((e) => console.error(e));
  }

  state: IState = {
    chatName: "My chat",
    participants: 0,
    messagesAmount: 0,
    lastMessageDate: "",
    pending: true,
    users: null,
    messages: [],
    currentUserId: "",
    messageInputValue: "",
    isEditing: false,
    editingMessageId: "",
  };

  onMessageInputValueChange(event: React.ChangeEvent<HTMLInputElement>) {
    const value: string = event.currentTarget.value;

    this.setState({ messageInputValue: value });
  }

  onMessageSend() {
    if (!this.state.messageInputValue.trim()) return;
    const userId = this.state.currentUserId;
    const userData = this.state.users!.get(userId);
    const createdAt = new Date().toISOString();

    const message: IMessage = {
      id: uuid(),
      text: this.state.messageInputValue,
      user: userData!.user,
      avatar: userData!.avatar,
      userId,
      likes: [],
      editedAt: "",
      createdAt,
    };

    this.state.messages.push(message);

    this.setState({
      messagesAmount: this.state.messages.length,
      lastMessageDate: getMessageTime(createdAt),
      messages: [...this.state.messages],
      messageInputValue: "",
    });

    const chatBox = document.querySelector(".chat__messages-box");
    chatBox!.scrollTop = chatBox!.scrollHeight;
    focusOnInput();
  }

  onEditFinish() {
    const message = this.state.messages.find(
      (m) => m.id === this.state.editingMessageId
    ) as IMessage;
    message.text = this.state.messageInputValue;

    this.setState({
      messageInputValue: "",
      isEditing: false,
      editingMessageId: "",
      messages: [...this.state.messages],
    });
  }

  onEdit(messageId: string) {
    const message = this.state.messages.find(
      (m) => m.id === messageId
    ) as IMessage;

    this.setState({
      messageInputValue: message.text,
      isEditing: true,
      editingMessageId: messageId,
    });
    focusOnInput();
  }

  onDelete(messageId: string) {
    const messages = this.state.messages.filter(
      (m: IMessage) => m.id !== messageId
    );

    this.setState({
      messages,
      messagesAmount: messages.length,
    });
  }

  onLike(messageId: string) {
    const message = this.state.messages.find(
      (m) => m.id === messageId
    ) as IMessage;
    if (message?.likes.includes(this.state.currentUserId)) {
      message.likes = message.likes.filter(
        (id) => id !== this.state.currentUserId
      );
    } else {
      message.likes?.push(this.state.currentUserId);
    }
    this.setState({ messages: [...this.state.messages] });
  }

  render() {
    return this.state.pending ? (
      "loading..."
    ) : (
      <div className="chat">
        <ChatHeader
          chatName={this.state.chatName}
          participants={this.state.participants}
          messages={this.state.messagesAmount}
          lastMessageDate={this.state.lastMessageDate}
        />
        <ChatMessages
          currentUserId={this.state.currentUserId}
          messages={this.state.messages}
          onEdit={this.onEdit.bind(this)}
          onDelete={this.onDelete.bind(this)}
          onLike={this.onLike.bind(this)}
        />
        <div className="chat__message-form">
          <input
            id="message-input"
            onChange={this.onMessageInputValueChange.bind(this)}
            value={this.state.messageInputValue}
            className="input"
            type="text"
            placeholder="Enter the message"
          />
          {this.state.isEditing ? (
            <button onClick={this.onEditFinish.bind(this)} className="button">
              Edit
            </button>
          ) : (
            <button onClick={this.onMessageSend.bind(this)} className="button">
              Send
            </button>
          )}
        </div>
      </div>
    );
  }
}

function focusOnInput() {
  document.getElementById("message-input")?.focus();
}

function getActiveUserId(map: Map<any, any>) {
  return Array.from(map)[0][0];
}

function sortDataByDate(data: IMessage[]) {
  data.sort((a: IMessage, b: IMessage) => {
    const dateB = (new Date(b.createdAt) as unknown) as number;
    const dateA = (new Date(a.createdAt) as unknown) as number;
    return dateA - dateB;
  });
}

function createUsersMap(data: IMessage[]) {
  const users = new Map();
  data.forEach((message: IMessage) => {
    message.likes = [];
    if (!users.has(message.userId)) {
      users.set(message.userId, { user: message.user, avatar: message.avatar });
    }
  });
  return users;
}

function getMessageTime(date: string) {
  const time = new Date(date);
  return `${time.getHours()}:${time.getMinutes()}`;
}
