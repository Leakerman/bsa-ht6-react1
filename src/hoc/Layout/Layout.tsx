import React, { Component, Fragment } from "react";
import "./Layout.scss";

export default class Layout extends Component {
  render() {
    return (
      <Fragment>
        <header className="header">Logo</header>
        <main className="main">{this.props.children}</main>
        <footer className="footer">
          <div className="copyright">Copyright</div>
        </footer>
      </Fragment>
    );
  }
}
